package com.ggkovalenko.rxdemo.model;

/**
 * @author gregory on 02.06.2018.
 */
public class CombineModel {

    private final int e;
    private final int f;

    public CombineModel(DigitsModel digits, DateModel date) {
        e = digits.getA() + digits.getB();
        f = digits.getC() + date.getDayOfWeek();
    }

    public int getE() {
        return e;
    }

    public int getF() {
        return f;
    }

}
