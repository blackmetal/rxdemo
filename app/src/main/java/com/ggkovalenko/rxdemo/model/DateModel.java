package com.ggkovalenko.rxdemo.model;

/**
 * @author gregory on 01.06.2018.
 */
public class DateModel {

    private final int dayOfWeek;

    public DateModel(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

}
