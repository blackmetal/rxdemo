package com.ggkovalenko.rxdemo.model;

/**
 * @author gregory on 01.06.2018.
 */
public class DigitsModel {

    private final int a;
    private final int b;
    private final int c;

    public DigitsModel(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

}
