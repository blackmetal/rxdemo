package com.ggkovalenko.rxdemo;

import com.ggkovalenko.rxdemo.model.DateModel;
import com.ggkovalenko.rxdemo.model.DigitsModel;

import java.util.Date;

import io.reactivex.functions.Function;

/**
 * @author gregory on 01.06.2018.
 */
public interface PartialState extends Function<ViewState, ViewState> {

    class DigitsModelLoading implements PartialState {
        @Override
        public ViewState apply(ViewState viewState) {
            return viewState.builder()
                    .setDigitsModelLoading(true)
                    .setDigitsModelError(false)
                    .build();
        }
    }

    class DigitsModelError implements PartialState {
        @Override
        public ViewState apply(ViewState viewState) {
            return viewState.builder()
                    .setDigitsModelError(true)
                    .setDigitsModelLoading(false)
                    .build();
        }
    }

    class DigitsModelSuccess implements PartialState {
        private final DigitsModel digitsModel;

        public DigitsModelSuccess(DigitsModel digitsModel) {
            this.digitsModel = digitsModel;
        }

        @Override
        public ViewState apply(ViewState viewState) {
            return viewState.builder()
                    .setDigitsModelError(false)
                    .setDigitsModelLoading(false)
                    .setDigitsModel(digitsModel)
                    .build();
        }
    }

    class DateModelLoading implements PartialState {
        private final Date date;

        public DateModelLoading(Date date) {
            this.date = date;
        }

        @Override
        public ViewState apply(ViewState viewState) {
            return viewState.builder()
                    .setDate(date)
                    .setDateModelLoading(true)
                    .setDateModelError(false)
                    .build();
        }
    }

    class DateModelError implements PartialState {
        @Override
        public ViewState apply(ViewState viewState) {
            return viewState.builder()
                    .setDateModelError(true)
                    .setDateModelLoading(false)
                    .build();
        }
    }

    class DateModelSuccess implements PartialState {
        private final DateModel dateModel;

        public DateModelSuccess(DateModel dateModel) {
            this.dateModel = dateModel;
        }

        @Override
        public ViewState apply(ViewState viewState) {
            return viewState.builder()
                    .setDateModelError(false)
                    .setDateModelLoading(false)
                    .setDateModel(dateModel)
                    .build();
        }
    }

}
