package com.ggkovalenko.rxdemo;

import android.support.annotation.Nullable;

import com.ggkovalenko.rxdemo.model.CombineModel;
import com.ggkovalenko.rxdemo.model.DateModel;
import com.ggkovalenko.rxdemo.model.DigitsModel;

import java.util.Date;

/**
 * @author gregory on 01.06.2018.
 */
public class ViewState {

    private final boolean digitsModelLoading;
    private final boolean digitsModelError;
    private final DigitsModel digitsModel;
    private final boolean dateModelLoading;
    private final boolean dateModelError;
    private final DateModel dateModel;
    private final Date date;

    public ViewState() {
        digitsModelLoading = false;
        digitsModelError = false;
        digitsModel = null;
        dateModelLoading = false;
        dateModelError = false;
        dateModel = null;
        date = new Date(); // today
    }

    private ViewState(Builder builder) {
        digitsModelLoading = builder.digitsModelLoading;
        digitsModelError = builder.digitsModelError;
        digitsModel = builder.digitsModel;
        dateModelLoading = builder.dateModelLoading;
        dateModelError = builder.dateModelError;
        dateModel = builder.dateModel;
        date = builder.date;
    }

    public boolean isLoading() {
        return digitsModelLoading || dateModelLoading;
    }

    public boolean isError() {
        return digitsModelError || dateModelError;
    }

    @Nullable
    public DigitsModel getDigitsModel() {
        return digitsModel;
    }

    @Nullable
    public DateModel getDateModel() {
        return dateModel;
    }

    @Nullable
    public CombineModel getCombineModel() {
        if (digitsModel != null && dateModel != null) {
            return new CombineModel(digitsModel, dateModel);
        }
        return null;
    }

    public Date getDate() {
        return date;
    }

    public Builder builder() {
        return new Builder(this);
    }

    public static class Builder {
        private boolean digitsModelLoading;
        private boolean digitsModelError;
        private DigitsModel digitsModel;
        private boolean dateModelLoading;
        private boolean dateModelError;
        private DateModel dateModel;
        private Date date;

        private Builder(ViewState viewState) {
            digitsModelLoading = viewState.digitsModelLoading;
            digitsModelError = viewState.digitsModelError;
            digitsModel = viewState.digitsModel;
            dateModelLoading = viewState.dateModelLoading;
            dateModelError = viewState.dateModelError;
            dateModel = viewState.dateModel;
            date = viewState.date;
        }

        public Builder setDigitsModelError(boolean digitsModelError) {
            this.digitsModelError = digitsModelError;
            return this;
        }

        public Builder setDigitsModelLoading(boolean digitsModelLoading) {
            this.digitsModelLoading = digitsModelLoading;
            return this;
        }

        public Builder setDigitsModel(DigitsModel digitsModel) {
            this.digitsModel = digitsModel;
            return this;
        }

        public Builder setDateModelError(boolean dateModelError) {
            this.dateModelError = dateModelError;
            return this;
        }

        public Builder setDateModelLoading(boolean dateModelLoading) {
            this.dateModelLoading = dateModelLoading;
            return this;
        }

        public Builder setDateModel(DateModel dateModel) {
            this.dateModel = dateModel;
            return this;
        }

        public Builder setDate(Date date) {
            this.date = date;
            return this;
        }



        public ViewState build() {
            return new ViewState(this);
        }
    }

}
