package com.ggkovalenko.rxdemo;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.ggkovalenko.rxdemo.model.CombineModel;

import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainView, DatePickerDialog.OnDateSetListener {

    @BindView(R.id.preloader) View preloaderView;
    @BindView(R.id.e) TextView eView;
    @BindView(R.id.f) TextView fView;
    @BindView(R.id.date) TextView dateView;

    private MainPresenter presenter;
    private DatePickerDialog datePickerDialog;

    @OnClick(R.id.refreshDigits)
    void onRefreshDigitsClick() {
        presenter.getDigitsModel();
    }

    @OnClick(R.id.pickDate)
    void onPickDateClick() {
        if (datePickerDialog != null) {
            datePickerDialog.show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new MainPresenter(new Repository(), this);
        presenter.getDigitsModel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void render(ViewState viewState) {
        if (viewState.isError()) {
            preloaderView.setVisibility(View.GONE);
            eView.setText("x");
            fView.setText("x");
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();

        } else if (viewState.isLoading()) {
            preloaderView.setVisibility(View.VISIBLE);
        } else {
            preloaderView.setVisibility(View.GONE);

            final CombineModel combine = viewState.getCombineModel();
            if (combine != null) {
                eView.setText(String.valueOf(combine.getE()));
                fView.setText(String.valueOf(combine.getF()));
            }

            final Calendar calendar = new GregorianCalendar();
            calendar.setTime(viewState.getDate());
            if (datePickerDialog == null) {
                datePickerDialog = new DatePickerDialog(this, this,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
            } else {
                datePickerDialog.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        final Calendar calendar = new GregorianCalendar(year, month, dayOfMonth);
        presenter.getDateModel(calendar.getTime());
    }

}
