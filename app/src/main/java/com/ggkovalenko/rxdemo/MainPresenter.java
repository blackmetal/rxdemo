package com.ggkovalenko.rxdemo;

import java.util.Date;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;

/**
 * @author gregory on 01.06.2018.
 */
public class MainPresenter {

    private final Repository repository;
    private final PublishProcessor<Boolean> digitsProcessor;
    private final PublishProcessor<Date> dateProcessor;
    private final Disposable disposable;

    public MainPresenter(Repository repository, final MainView view) {
        this.repository = repository;
        digitsProcessor = PublishProcessor.create();
        dateProcessor = PublishProcessor.create();

        final Flowable<PartialState> digitsStates = digitsProcessor.switchMap(this::getDigitsFlowable);
        final Flowable<PartialState> dateStates = dateProcessor.switchMap(this::getDateFlowable);
        disposable = Flowable.merge(digitsStates, dateStates)
                .scan(new ViewState(), (viewState, partialState) -> partialState.apply(viewState))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::render, throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    private Flowable<PartialState> getDigitsFlowable(Boolean value) {
        return repository.getDigitsModel()
                .subscribeOn(Schedulers.io())
                .toFlowable()
                .<PartialState>map(PartialState.DigitsModelSuccess::new)
                .startWith(new PartialState.DigitsModelLoading())
                .onErrorReturn(throwable -> new PartialState.DigitsModelError());
    }

    private Flowable<PartialState> getDateFlowable(final Date date) {
        return repository.getDateModel(date)
                .subscribeOn(Schedulers.io())
                .toFlowable()
                .<PartialState>map(PartialState.DateModelSuccess::new)
                .startWith(new PartialState.DateModelLoading(date))
                .onErrorReturn(throwable -> new PartialState.DateModelError());
    }

    public void getDigitsModel() {
        digitsProcessor.onNext(Boolean.TRUE);
    }

    public void getDateModel(final Date date) {
        dateProcessor.onNext(date);
    }

    public void onDestroy() {
        disposable.dispose();
    }

}
