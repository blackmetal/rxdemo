package com.ggkovalenko.rxdemo;

/**
 * @author gregory on 01.06.2018.
 */
public interface MainView {

    void render(ViewState viewState);

}
