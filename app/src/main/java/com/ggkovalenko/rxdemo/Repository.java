package com.ggkovalenko.rxdemo;

import com.ggkovalenko.rxdemo.model.DateModel;
import com.ggkovalenko.rxdemo.model.DigitsModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Random;

import io.reactivex.Single;

/**
 * @author gregory on 01.06.2018.
 */
public class Repository {

    private final Random random;
    private final DateFormat dateFormat;

    public Repository() {
        random = new Random();
        dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.getDefault());
    }

    public Single<DigitsModel> getDigitsModel() {
        return Single.create(emitter -> {
            try {
                Thread.sleep(random.nextInt(3500));
                if (random.nextInt(4) == 0) {
                    throw new Exception("This is error case");
                }
                emitter.onSuccess(new DigitsModel(random.nextInt(10),
                        random.nextInt(10), random.nextInt(10)));
            } catch (InterruptedException ie) {
                // ignore
            } catch (Throwable throwable) {
                emitter.onError(throwable);
            }
        });
    }

    public Single<DateModel> getDateModel(final Date date) {
        return Single.create(emitter -> {
            try {
                Thread.sleep(random.nextInt(3500));
                if (random.nextInt(4) == 0) {
                    throw new Exception("This is error case");
                }
                final Calendar calendar = new GregorianCalendar();
                calendar.setTime(date);
                emitter.onSuccess(new DateModel(calendar.get(Calendar.DAY_OF_WEEK)));
            } catch (InterruptedException ie) {
                // ignore
            } catch (Throwable throwable) {
                emitter.onError(throwable);
            }
        });
    }

}
